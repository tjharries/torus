#include "pparameterdefinition.h"

PParameterDefinition::PParameterDefinition()
{
    this->decimals = 2;
    this->min = 0;
    this->max = 1e10;
}
