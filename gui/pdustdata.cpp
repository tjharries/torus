#include "pdustdata.h"

PDustData::PDustData()
{
    amin = 0.005;
    amax = 0.25;
    qdist = 3.5;
    a0 = 1.0e20;
    pdist = 1;
    grainfrac = 1;
    graintype = "sil_dl";
}
