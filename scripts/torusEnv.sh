# Set up paths for Torus under bash-like shells
# Call with: source scripts/torusEnv.sh
this_dir=`pwd`
export PATH=${this_dir}/utilties/python:${this_dir}/bin:${this_dir}/scripts:${PATH}
