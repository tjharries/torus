module vh1_mod

  use kind_mod

  implicit none

! Public components
  public :: read_vh1, assign_from_vh1, get_density_vh1, vh1FileRequired, &
       setGridFromVh1Parameters, deallocate_vh1
  
  private

! Private components
  integer, private :: nx
  integer, private :: ny
  real(db), private, save, allocatable :: rho(:,:)  ! VH-1 density
  real(db), private, save, allocatable :: tem(:,:)  ! VH-1 temperature
  real(db), private, save, allocatable :: xaxis(:)  ! VH-1 x-axis
  real(db), private, save, allocatable :: yaxis(:)  ! VH-1 y-axis
  real(db), private, save, allocatable :: vx(:,:)   ! VH-1 x velocity
  real(db), private, save, allocatable :: vy(:,:)   ! VH-1 y velocity

  real(db), private, save :: xOffset, yOffset
  logical, private  :: isRequired=.false.
  character(len=80), private :: infile

! Density for regions outside the VH-1 grid
  real(db), parameter, private :: rho_bg=1.0e-23_db

contains

! Set module variables from values the parameters file. Called from inputs_mod.
  subroutine setGridFromVh1Parameters(vh1filename, vh1xOffset, vh1yOffset)
    character(len=80), intent(in) :: vh1filename
    real(double), intent(in) :: vh1xOffset, vh1yOffset

    infile     = vh1filename
    xOffset    = vh1xOffset
    yOffset    = vh1yOffset
    isRequired = .true.

  end subroutine setGridFromVh1Parameters

! Accessor function
  logical function  vh1FileRequired()
     vh1FileRequired=isRequired
  end function vh1FileRequired

  subroutine read_vh1
    
    ! Read data from VH-1 output file written by printold.f
    
    use messages_mod
    use constants_mod, only: kErg, mHydrogen

    implicit none

    character(len=7) :: head1
    character(len=6) :: head2
    character(len=4) :: head3
    integer :: vh1_cycle
    real :: time, dt

    real(db) :: pres
    real(db) :: numDen ! Number density
    real(db),parameter :: meanMassPerParticle = (14.0/23.0) * mHydrogen
    integer :: i, j
    integer :: nlines

    character(len=200) message

! Work out the size of the grid and allocate storage. 
! nlines includes one header line i.e.
! nx = sqrt(l+1) - 1 
! if l is lines of data only

    nlines = file_line_count(infile)
    nx = int(sqrt(real(nlines))) - 1
    ny = int(sqrt(real(nlines))) - 1

    write(message,*) "Found ", nlines, " lines in ", infile
    call writeInfo(message, FORINFO)
    write(message,*) "Assuming grid is square: nx, ny= ", nx, ny
    call writeInfo(message, FORINFO)

    allocate(rho(nx,ny))
    allocate(tem(nx,ny))
    allocate(vx(nx,ny))
    allocate(vy(nx,ny))
    allocate(xaxis(nx))
    allocate(yaxis(ny))

    open(unit=10, status="old", file=infile)
    
! Open file
    write(message,*) "Opening file "//infile
    call writeInfo(message, FORINFO)

! Read header
    read(10,*) head1, vh1_cycle, head2, time, head3, dt
    write(message,*) "Header: ", head1, vh1_cycle, head2, time, head3, dt
    call writeInfo(message, TRIVIAL)
    
! Read vh1 data values
    do j=1, ny
       do i=1, nx
          read(10,*) rho(i,j), pres, vy(i,j), vx(i,j)
          numDen = rho(i,j) / meanMassPerParticle
          tem(i,j) = pres/(kErg*numDen)
       end do
    end do

! Read x-axis values
    do i=1, nx
       read(10,*) xaxis(i)
    end do

! Read y-axis values
    do j=1, ny
       read(10,*) yaxis(j)
    end do

! Convert axes to Torus units
    xaxis(:) = xaxis(:) / 1.0e10_db
    yaxis(:) = yaxis(:) / 1.0e10_db

! Apply offsets.
    if (xOffset /= 0.0) then 
       xaxis(:) = xaxis(:) + xOffset
       write(message,*) "X-axis offset by ", xOffset, " (Torus length units)"
       call writeInfo(message, FORINFO)
    end if

    if (yOffset /= 0.0) then 
       yaxis(:) = yaxis(:) + yOffset
       write(message,*) "Y-axis offset by ", xOffset, " (Torus length units)"
       call writeInfo(message, FORINFO)
    end if

    close(10)

    call writeInfo("Finished reading VH-1 data", FORINFO)
    write(message,*) "x-axis (min/max) =", xaxis(1), xaxis(nx)
    call writeInfo(message, FORINFO)
    write(message,*) "y-axis (min/max) =", yaxis(1), yaxis(nx)
    call writeInfo(message, FORINFO)

  contains

! Count the number of lines in a file. 
! Should return the same answer as wc -l i.e. includes blank lines in the total
! D. Acreman, June 2010
    integer function file_line_count(filename)

      character(len=*) :: filename
      character(len=1) :: dummy
      integer :: status 

      file_line_count = 0
      open(unit=30, status="old", file=filename)
      do
         read(30,'(a1)',iostat=status) dummy
         if ( status /= 0 ) exit
         file_line_count = file_line_count + 1 
      end do
      close(30)

    end function file_line_count

  end subroutine read_vh1

  subroutine assign_from_vh1(thisOctal, subcell)

    use octal_mod
    use vector_mod
    implicit none

    TYPE(OCTAL), intent(inout) :: thisOctal
    integer, intent(in) :: subcell

    TYPE(vector) :: thisVel, thisCentre
    integer :: this_i, this_j, i, j


    thisCentre = subcellcentre(thisOctal, subcell)

! Remember that the axes are flipped. 
    if ( thisCentre%x < yaxis(1)  .or. &
         thisCentre%x > yaxis(nx) .or. &
         thisCentre%z < xaxis(1)  .or. &
         thisCentre%z > xaxis(ny) ) then 
       
       thisOctal%rho(subcell)      = rho_bg
       thisOctal%velocity(subcell) = VECTOR(0.0,0.0,0.0)

    else

       do i=2, nx
          if ( thisCentre%x < yaxis(i) ) then 
             this_i = i
             exit
          end if
       end do

! In a 2D grid the octal y value doesn't change
       do j=2, ny
          if ( thisCentre%z < xaxis(j) ) then 
             this_j = j
             exit
          end if
       end do

! this_j is for VH-1 x-axis and this_i is for VH-1 y-axis
       thisOctal%rho(subcell)         = rho(this_j, this_i)
       thisOctal%temperature(subcell) = real(tem(this_j, this_i))

! Extract cell velocity from the VH-1 grid and store velocity as fraction of c. 
       thisVel=VECTOR(vy(this_j, this_i), 0.0, vx(this_j, this_i))
       thisOctal%velocity(subcell) = thisVel/cspeed

    end if

  end subroutine assign_from_vh1

  subroutine get_density_vh1(thisOctal, subcell, mean_rho, min_rho, max_rho, ncells)

    use octal_mod

    implicit none 

    TYPE(OCTAL), intent(in) :: thisOctal
    integer, intent(in)     :: subcell
    real(db), intent(out)   :: mean_rho
    real(db), intent(out)   :: min_rho
    real(db), intent(out)   :: max_rho
    integer, intent(out)    :: ncells

    integer :: imin, imax, jmin, jmax
    integer :: i, j
    TYPE(VECTOR) :: cellcentre
    real(db) :: this_loc, sum_rho

! Find cells which are in the octal
! Not efficient for a uniform grid but will work for the non-uniform case
! Remember that: Torus x-axis -> VH-1 y-axis
!                Torus z-axis -> VH-1 x-axis

    cellCentre = subcellCentre(thisOctal,subCell)

    this_loc = cellCentre%z - (0.5 * thisOctal%subcellsize)
    imin = -99
    do i=1, nx
       ! find the first vh1 point within this subcell
       if ( xaxis(i) > this_loc ) then
          imin = i 
          exit
       end if
    end do
    if ( imin == -99 ) then 
       mean_rho = 0.0
       min_rho  = 0.0
       max_rho  = 0.0
       ncells   = -1
       return
    end if

    this_loc = cellCentre%z + (0.5 * thisOctal%subcellsize)
    imax = -99
    do i=nx, 1, -1
       ! find the first vh1 point within this subcell
       if ( xaxis(i) < this_loc ) then
          imax = i 
          exit
       end if
    end do
    if ( imax == -99 ) then 
       mean_rho = 0.0
       min_rho  = 0.0
       max_rho  = 0.0
       ncells   = -2
       return
    end if

    this_loc = cellCentre%x - (0.5 * thisOctal%subcellsize)
    jmin = -99
    do j=1, ny
       ! find the first vh1 point within this subcell
       if ( yaxis(j) > this_loc ) then
          jmin = j 
          exit
       end if
    end do
    if ( jmin == -99 ) then 
       mean_rho = 0.0
       min_rho  = 0.0
       max_rho  = 0.0
       ncells   = -3
       return
    end if

    this_loc = cellCentre%x + (0.5 * thisOctal%subcellsize)
    jmax = -99
    do j=ny, 1, -1
       ! find the first vh1 point within this subcell
       if ( yaxis(j) < this_loc ) then
          jmax = j 
          exit
       end if
    end do
    if ( jmax == -99 ) then 
       mean_rho = 0.0
       min_rho  = 0.0
       max_rho  = 0.0
       ncells   = -4
       return
    end if

    max_rho = -1.0e30_db
    min_rho =  1.0e30_db
    sum_rho =  0.0 
    ncells  =  0 
    do j=jmin, jmax
       do i=imin, imax
          max_rho = max(max_rho, rho(i,j) )
          min_rho = min(min_rho, rho(i,j) )
          sum_rho = sum_rho + rho(i,j)
          ncells  = ncells + 1 
       end do
    end do
    mean_rho = sum_rho / real(ncells,db)

  end subroutine get_density_vh1

  subroutine deallocate_vh1
    
    if (allocated(rho))   deallocate(rho)
    if (allocated(tem))   deallocate(tem)
    if (allocated(vx))    deallocate(vx)
    if (allocated(vy))    deallocate(vy)
    if (allocated(xaxis)) deallocate(xaxis)
    if (allocated(yaxis)) deallocate(yaxis)

  end subroutine deallocate_vh1

end module vh1_mod
