
module cmfgen_class
  ! --------------------------------------------------------------------------------
  !  This module uses the OPACITY_DATA output file from CMFGEN of J. Hillier to
  !  to map the opacity and denisty (it does not matter, but assuming proportional 
  !  to the square root of the thermal opacity).
  !  AND MORE COMMENTS TO BE INSERTED LATER.
  !---------------------------------------------------------------------------------
  !  Created: mar-24-2005  (R.Kurosawa)
  !---------------------------------------------------------------------------------

  use kind_mod
  use messages_mod
  use constants_mod
  use vector_mod
  use gridtype_mod
  use octal_mod, only: OCTAL, subcellCentre
  use utils_mod, only: loginterp_dble

  IMPLICIT NONE
    
  public :: &
       read_cmfgen_data, &
       deallocate_cmfgen_data, &
       cmfgen_density, &
       cmfgen_velocity, &
       cmfgen_mass_velocity, &
       calc_cmfgen_temperature, &
       get_cmfgen_nd,&
       get_cmfgen_data_array, &
       get_cmfgen_data_array_element, &
       put_cmfgen_Rmin, &
       get_cmfgen_Rmin, &
       put_cmfgen_Rmax, &
       get_cmfgen_Rmax, &
       distort_cmfgen
       


  private :: &
       VelocityCorners

  
  
  type cmfgen_data
     private
     real(double)      :: Rmin          ! Minimum radius  [10^10cm]
     real(double)      :: Rmax          ! Maximum radius  [10^10cm]
     ! header info in the OPACITY_DATA
     character(LEN=12) :: format_date ! Revised format date
     character(LEN=12) :: model_id    ! Model ID
     character(LEN=12) :: trans_id    ! Transition ID
     logical           :: diff_aprox  ! if T diffusiton approx use at inner boundary
     real(double)      :: lambda      ! wavelength of the line
     real(double)      :: amass       ! atomic mass
     real(double)      :: Ic          ! Schuster intensity
     integer           :: nd          ! Number of depth points
     ! The following arrays should be allocated with size = nd
     real(double), pointer  :: R(:)      ! radial distance  [10^cm]
     real(double), pointer  :: T(:)      ! temperature [K]
     real(double), pointer  :: sigma(:)  ! 
     real(double), pointer  :: V(:)      ! radial velocity [km/s]
     real(double), pointer  :: rho(:)      ! mass density
     real(double), pointer  :: eta(:)    ! thermal emissivity  [ ]
     real(double), pointer  :: chi_th(:) ! thermal opacity     [ ]
     real(double), pointer  :: esec(:)   ! electron scattering opacity []
     real(double), pointer  :: etal(:)   ! line emissivity []
     real(double), pointer  :: chil(:)   ! line emissivity []     
  end type cmfgen_data


  !
  ! One instance to be used ONLY IN THIS MODULE.
  ! This is a work around since to use Neil's generic rouines (calcValuesAMR and  finishGrid  
  ! rouine.  (This acts like an common block! This should be stopped in the future!)
  type(cmfgen_data), save :: cmfgen_opacity


  
contains


  ! 
  ! CONSTRUCTOR
  !
  ! -- Intialize the object and read the data.
  subroutine read_cmfgen_data(filename)
    use utils_mod, only : reverse
    implicit none
    character(LEN=*), intent(in) :: filename ! File name for velocity and density    
    !
    character(LEN=12) :: dum_a
    character(LEN=120) :: dum_a_long
    integer :: i, nd
    integer :: luin =88
    real(double) :: freq, ln_ri, ln_rmax, ln_rmin, dln_r, rstar
    characteR(len=120) :: msg

    open(unit=luin, file=filename, status="old")

    if (filename == "OPACITY_DATA") then
       
       ! Reading header and writing it to the standard output for record.
       call writeInfo(" ")
       call writeInfo(" ")
       call writeInfo("======= Reading OPACITY_DATA of CMFGEN =================================== ")
       do i = 1, 9       
          read(luin,'(a)') dum_a_long       
          write(msg,'(a)') dum_a_long
          call writeInfo(msg)
       end do
       call writeInfo("========================================================================== ")
       call writeInfo(" ")
       call writeInfo(" ")
       
       
       
       ! now reading the data and storing them
       rewind(luin)
       read(luin,*) cmfgen_opacity%format_date
       read(luin,*) cmfgen_opacity%model_id
       read(luin,*) cmfgen_opacity%trans_id
       read(luin,*) cmfgen_opacity%diff_aprox
       read(luin,*) freq
       read(luin,*) cmfgen_opacity%lambda
       read(luin,*) cmfgen_opacity%amass
       read(luin,*) cmfgen_opacity%Ic
       read(luin,*) cmfgen_opacity%nd
       read(luin,*) dum_a  ! skip header    
       ! now allocate memory for data arrays
       nd = cmfgen_opacity%nd
       ALLOCATE(cmfgen_opacity%r(nd), cmfgen_opacity%T(nd), cmfgen_opacity%sigma(nd), cmfgen_opacity%V(nd))
       ALLOCATE(cmfgen_opacity%eta(nd), cmfgen_opacity%chi_th(nd), cmfgen_opacity%esec(nd), &
            cmfgen_opacity%etal(nd), cmfgen_opacity%chil(nd), cmfgen_opacity%rho(nd))
       !initialze data arrays
       cmfgen_opacity%r(1:nd)=0.0d0; cmfgen_opacity%T(1:nd)=0.0d0; cmfgen_opacity%sigma(1:nd)=0.0d0
       cmfgen_opacity%V(1:nd)=0.0d0; cmfgen_opacity%eta(1:nd)=0.0d0; cmfgen_opacity%chi_th(1:nd)=0.0d0
       cmfgen_opacity%esec(1:nd)=0.0d0; cmfgen_opacity%etal(1:nd)=0.0d0; cmfgen_opacity%chil(1:nd)=0.0d0
       cmfgen_opacity%rho(1:nd)=0.0d0
       
       do i = 1, nd
          read(luin, *)              &
               cmfgen_opacity%r(nd+1-i),       &
               cmfgen_opacity%T(nd+1-i),       &
               cmfgen_opacity%sigma(nd+1-i),   &
               cmfgen_opacity%V(nd+1-i),       &
               cmfgen_opacity%eta(nd+1-i),     &
               cmfgen_opacity%chi_th(nd+1-i),  &
               cmfgen_opacity%esec(nd+1-i),    &
               cmfgen_opacity%etal(nd+1-i),    &
               cmfgen_opacity%chil(nd+1-i)
       end do
       
       ! changing units of T from 10^4 K to K
       cmfgen_opacity%T(1:nd) = 1.0d4*cmfgen_opacity%T(1:nd)
       

!XXXXXXXXXXXXXXXXXXXXXX
!just testing 
!       cmfgen_opacity%chil(1:nd) = 0.5d0*cmfgen_opacity%chil(1:nd)  ! adding missing factor if hydrogen


       !
       ! Assigining a psedo density using electron scattering opacity!
       cmfgen_opacity%rho(1:nd) = cmfgen_opacity%esec(1:nd)

       
    else if (filename == "RVTJ") then
       ! Reading header and writing it to the standard output for record.
       call writeInfo(" ")
       call writeInfo(" ")
       call writeInfo("======= Reading RVTJ of CMFGEN =================================== ")
       do i = 1, 12      
          read(luin,'(a)') dum_a_long       
          write(msg,'(a)') dum_a_long
          call writeInfo(msg)
       end do
       call writeInfo("========================================================================== ")
       call writeInfo(" ")
       call writeInfo(" ")
       
       
       
       ! now reading the data and storing them
       rewind(luin)
       read(luin,*) cmfgen_opacity%format_date
       read(luin,*) cmfgen_opacity%model_id
       read(luin,*) dum_a  ! skip header    
       read(luin,*) dum_a, cmfgen_opacity%nd
       read(luin,*) dum_a  ! skip header    
       read(luin,*) dum_a  ! skip header    
       read(luin,*) dum_a  ! skip header    
       read(luin,*) dum_a  ! skip header    
       read(luin,*) dum_a  ! skip header    
       read(luin,*) dum_a  ! skip header    
       read(luin,*) dum_a  ! skip header    
       read(luin,*) dum_a  ! skip header    

       ! now allocate memory for data arrays
       nd = cmfgen_opacity%nd
       ALLOCATE(cmfgen_opacity%r(nd), cmfgen_opacity%T(nd), cmfgen_opacity%sigma(nd), cmfgen_opacity%V(nd))
       ALLOCATE(cmfgen_opacity%eta(nd), cmfgen_opacity%chi_th(nd), cmfgen_opacity%esec(nd), &
            cmfgen_opacity%etal(nd), cmfgen_opacity%chil(nd), cmfgen_opacity%rho(nd))
       ! Initializing the data arrays
       cmfgen_opacity%r(1:nd)=0.0d0; cmfgen_opacity%T(1:nd)=0.0d0; cmfgen_opacity%sigma(1:nd)=0.0d0
       cmfgen_opacity%V(1:nd)=0.0d0; cmfgen_opacity%eta(1:nd)=0.0d0; cmfgen_opacity%chi_th(1:nd)=0.0d0
       cmfgen_opacity%esec(1:nd)=0.0d0; cmfgen_opacity%etal(1:nd)=0.0d0; cmfgen_opacity%chil(1:nd)=0.0d0
       cmfgen_opacity%rho(1:nd)=0.0d0


       ! Read radius
       read(luin,*) dum_a  
       read(luin,*) cmfgen_opacity%r(1:nd)   ! 10^10cm

       ! Read Velocity
       read(luin,*) dum_a  
       read(luin,*) cmfgen_opacity%V(1:nd)   ! km/s

       ! Read sigma
       read(luin,*) dum_a  
       read(luin,*) cmfgen_opacity%sigma(1:nd)   ! 

       ! Read electron density to a dummy (eta array)
       read(luin,*) dum_a  
       read(luin,*) cmfgen_opacity%eta(1:nd)   ! 

       ! Read temeprature
       read(luin,*) dum_a  
       read(luin,*) cmfgen_opacity%T(1:nd)   ! 

       ! Read Rosseland Mean Opacity to a dummy (eta array)
       read(luin,*) dum_a  
       read(luin,*) cmfgen_opacity%eta(1:nd)   ! 

       ! Read Flux Mean Opacity to a dummy (eta array)
       read(luin,*) dum_a  
       read(luin,*) cmfgen_opacity%eta(1:nd)   ! 

       ! Read Atom Density to a dummy (eta array)
       read(luin,*) dum_a  
       read(luin,*) cmfgen_opacity%eta(1:nd)   ! 

       ! Read Ion Density to a dummy (eta array)
       read(luin,*) dum_a  
       read(luin,*) cmfgen_opacity%eta(1:nd)   ! 

       ! Read density
       read(luin,*) dum_a  
       read(luin,*) cmfgen_opacity%rho(1:nd)   !  [g/cm^3]

       ! Now reverse the order

       ! Note using eta array for tmp array       
       cmfgen_opacity%eta(1:nd) = cmfgen_opacity%r(1:nd)
       do i = 1, nd
          cmfgen_opacity%r(i) = cmfgen_opacity%eta(nd+1-i)
       end do

       cmfgen_opacity%eta(1:nd) = cmfgen_opacity%V(1:nd)
       do i = 1, nd
          cmfgen_opacity%V(i) = cmfgen_opacity%eta(nd+1-i)
       end do

       cmfgen_opacity%eta(1:nd) = cmfgen_opacity%sigma(1:nd)
       do i = 1, nd
          cmfgen_opacity%sigma(i) = cmfgen_opacity%eta(nd+1-i)
       end do

       cmfgen_opacity%eta(1:nd) = cmfgen_opacity%T(1:nd)
       do i = 1, nd
          cmfgen_opacity%T(i) = cmfgen_opacity%eta(nd+1-i)
       end do

       cmfgen_opacity%eta(1:nd) = cmfgen_opacity%rho(1:nd)
       do i = 1, nd
          cmfgen_opacity%rho(i) = cmfgen_opacity%eta(nd+1-i)
       end do
       cmfgen_opacity%eta(1:nd) = 0.0d0


!       print *, "r   = ", cmfgen_opacity%r
!       print *, "Vr  = ", cmfgen_opacity%V
!       print *, "T   = ", cmfgen_opacity%T
!       print *, "rho = ", cmfgen_opacity%rho


       
       ! changing units of T from 10^4 K to K
       cmfgen_opacity%T(1:nd) = 1.0d4*cmfgen_opacity%T(1:nd)

    else if (filename == "DUMMY") then
       ! sets up the data with functions in this routine

       !  Number of grid points
       nd = 70 
       cmfgen_opacity%nd = nd

       ! allocate arrays

       ALLOCATE(cmfgen_opacity%r(nd), cmfgen_opacity%T(nd), cmfgen_opacity%sigma(nd), cmfgen_opacity%V(nd))
       ALLOCATE(cmfgen_opacity%eta(nd), cmfgen_opacity%chi_th(nd), cmfgen_opacity%esec(nd), &
            cmfgen_opacity%etal(nd), cmfgen_opacity%chil(nd), cmfgen_opacity%rho(nd))
       ! Initializing the data arrays
       cmfgen_opacity%r(1:nd)=0.0d0; cmfgen_opacity%T(1:nd)=0.0d0; cmfgen_opacity%sigma(1:nd)=0.0d0
       cmfgen_opacity%V(1:nd)=0.0d0; cmfgen_opacity%eta(1:nd)=0.0d0; cmfgen_opacity%chi_th(1:nd)=0.0d0
       cmfgen_opacity%esec(1:nd)=0.0d0; cmfgen_opacity%etal(1:nd)=0.0d0; cmfgen_opacity%chil(1:nd)=0.0d0
       cmfgen_opacity%rho(1:nd)=0.0d0



       !----------------------------------
       Rstar = 139.10d0  ! 10^10 cm
       !----------------------------------


       ! Set up R grid ! spacing with natural log.
       ln_Rmin =  log( cmfgen_opacity%Rmin )
       ln_Rmax =  log( cmfgen_opacity%Rmax )
       dln_R =  ln_Rmax - ln_Rmin

       do i = 1, nd 
          ln_ri = ln_Rmin + dln_R*dble(i-1)/dble(nd-1)
          cmfgen_opacity%r(i) = EXP(ln_ri)          ! 10^10 cm
       end do

       ! set up velocity, density and temperature
       do i = 1, nd 
          cmfgen_opacity%V(i)   = cmfgen_beta_velocity(cmfgen_opacity%r(i), Rstar)     ! km/s
          cmfgen_opacity%rho(i) = cmfgen_beta_density(cmfgen_opacity%r(i), Rstar)      ! g/cm^3
          cmfgen_opacity%T(i)   = cmfgen_beta_temperature(cmfgen_opacity%r(i), Rstar)  ! K
       end do

       ! set up other data here.
       ! They can be just left as they are (zeros)
              
    else
       write(msg,'(a)') "Error: Unknown filename was passed to read_cmfgen_data."
       stop
    end if
    close(luin)



  end subroutine read_cmfgen_data
  


  !
  ! -- Deallocate memory for the data arrays
  !
  subroutine deallocate_cmfgen_data()
    implicit none
    !
    if (ASSOCIATED(cmfgen_opacity%r)) DEALLOCATE(cmfgen_opacity%r); NULLIFY(cmfgen_opacity%r)
    if (ASSOCIATED(cmfgen_opacity%T)) DEALLOCATE(cmfgen_opacity%T); NULLIFY(cmfgen_opacity%T)
    if (ASSOCIATED(cmfgen_opacity%sigma)) DEALLOCATE(cmfgen_opacity%sigma); NULLIFY(cmfgen_opacity%sigma)
    if (ASSOCIATED(cmfgen_opacity%V)) DEALLOCATE(cmfgen_opacity%V); NULLIFY(cmfgen_opacity%V)
    if (ASSOCIATED(cmfgen_opacity%eta)) DEALLOCATE(cmfgen_opacity%eta); NULLIFY(cmfgen_opacity%eta)
    if (ASSOCIATED(cmfgen_opacity%chi_th)) DEALLOCATE(cmfgen_opacity%chi_th); NULLIFY(cmfgen_opacity%chi_th)
    if (ASSOCIATED(cmfgen_opacity%esec)) DEALLOCATE(cmfgen_opacity%esec); NULLIFY(cmfgen_opacity%esec)
    if (ASSOCIATED(cmfgen_opacity%etal)) DEALLOCATE(cmfgen_opacity%etal); NULLIFY(cmfgen_opacity%etal)
    if (ASSOCIATED(cmfgen_opacity%chil)) DEALLOCATE(cmfgen_opacity%chil); NULLIFY(cmfgen_opacity%chil)
    !
  end subroutine deallocate_cmfgen_data



  !
  ! ACCESSORS
  !
  !

  ! reruns the number depth
  function get_cmfgen_nd() RESULT(out)
    integer :: out
    out = cmfgen_opacity%nd
  end function get_cmfgen_nd


  ! rerturn named data array
  subroutine get_cmfgen_data_array(name, array)
    implicit none
    character(LEN=*), intent(in) :: name 
    real(double), intent(inout) :: array(:)
    integer :: nd
    nd = size(array)
    if (nd< cmfgen_opacity%nd) then
       print *, "Error:: nd < cmfgen_opacity%nd in [cmfgen_class::get_cmfgen_data_array]."
       print *, "                       nd = ", nd
       print *, "        cmfgen_opacity%nd = ", cmfgen_opacity%nd
       stop
    end if
    nd = cmfgen_opacity%nd
    !
    select case (name)
    case ("R")
       array(1:nd) = cmfgen_opacity%R(1:nd)
    case ("T")
       array(1:nd) = cmfgen_opacity%T(1:nd)
    case ("sigma")
       array(1:nd) = cmfgen_opacity%sigma(1:nd)
    case ("V")
       array(1:nd) = cmfgen_opacity%V(1:nd)
    case ("eta")
       array(1:nd) = cmfgen_opacity%eta(1:nd)
    case ("chi_th")
       array(1:nd) = cmfgen_opacity%chi_th(1:nd)
    case ("esec")
       array(1:nd) = cmfgen_opacity%esec(1:nd)
    case ("etal")
       array(1:nd) = cmfgen_opacity%etal(1:nd)
    case ("chil")
       array(1:nd) = cmfgen_opacity%chil(1:nd)
    case ("rho")
       array(1:nd) = cmfgen_opacity%rho(1:nd)
    case default
       print *, "Error:: Unknown name passed to [cmfgen_class::get_cmfgen_data_array]."
       print *, "       name = ", name
       stop
    end select

  end subroutine get_cmfgen_data_array

  !
  ! get i-th componet of a named array
  function get_cmfgen_data_array_element(name, i) RESULT(out)
    implicit none
    real(double) :: out
    character(LEN=*), intent(in) :: name 
    integer, intent(in) :: i 

    !
    select case (name)
    case ("R")
       out = cmfgen_opacity%R(i)
    case ("T")
       out = cmfgen_opacity%T(i)
    case ("sigma")
       out = cmfgen_opacity%sigma(i)
    case ("V")
       out = cmfgen_opacity%V(i)
    case ("chi_th")
       out = cmfgen_opacity%chi_th(i)
    case ("esec")
       out = cmfgen_opacity%esec(i)
    case ("etal")
       out = cmfgen_opacity%etal(i)
    case ("chil")
       out = cmfgen_opacity%chil(i)
    case ("rho")
       out = cmfgen_opacity%rho(i)
    case default
       print *, "Error:: Unknown name passed to [cmfgen_class::get_cmfgen_data_array]."
       print *, "       name = ", name
       stop
    end select

  end function get_cmfgen_data_array_element



       
  !
  !
  !===================================================================
  ! Given a point and grid, this function returns the density in [g] 
  ! at the point by interpolating the original zeus data.
  !    
  FUNCTION cmfgen_density(point) RESULT(out)
    
    IMPLICIT NONE

    REAL(double)                  :: out
    TYPE(Vector), INTENT(IN) :: point    
    !
    real(double), parameter ::  rho_min = 1.0e-18 ! [g/cm^3]     
    !
    real(double) :: ri

    ri = MODULUS(point)

    if (ri<cmfgen_opacity%Rmin .or. ri>cmfgen_opacity%Rmax) then 
       out = rho_min  ! just assign a small value and return
    else       
       ! using a routine in utils_mod
       out = loginterp_dble(cmfgen_opacity%rho, cmfgen_opacity%nd, cmfgen_opacity%R, ri)    
    end if

  END FUNCTION cmfgen_density



  !===================================================================
  ! Given a point and grid, this function returns
  ! the wind velocity (as a vector) in units of speed of light [c]. 
  ! 
  !====================================================================
  FUNCTION cmfgen_velocity(point) RESULT(out)
    
    use inputs_mod, only : bigOmega, eddingtonGamma, uniformStar
    IMPLICIT NONE
    type(vector)                  :: out   ! [c]
    TYPE(Vector), INTENT(IN) :: point
    type(vector) :: v
    ! Point in spherical coordinates
    real(double) :: r, Vr, mu, vfac, sintheta

    r = modulus(point)
    !
    if (r /= 0.d0) then
       mu = point%z/r
    else
       mu = 1.d0
    endif
    sinTheta = sqrt(1.d0-mu**2)


! Dwarkadas and Owocki, 2002, ApJ 581, 1337

    if ( uniformStar ) then
! uniform star

       vFac = (1.d0 - (bigOmega**2 * sinTheta**2)/(1.d0-eddingtonGamma))**0.5d0 ! eq 7 

    else
! grav darkened star

       vFac = (1.d0 - (bigOmega**2 * sinTheta**2))**0.5d0 ! eq 7 
    endif


    if (r<cmfgen_opacity%Rmin) then ! inside of the star
       v = VECTOR(1.0e-10, 1.0e-10, 1.0e-10)  ! [c]
    else
       Vr = loginterp_dble(cmfgen_opacity%V, cmfgen_opacity%nd, cmfgen_opacity%R, r)    
       if (r/=0.d0) then
          v = (point/r) * (Vr*1.0e5/cspeed_dbl)   ! [c]
       else
          v = VECTOR(0.d0, 0.d0, 0.d0)
       endif
       !     \hat(r) x  Vr/c
    end if


    out = vFac * v 
    
  END FUNCTION cmfgen_velocity


  !
  !
  !
  !===============================================================
  !  Returns given point and grid, this function return 
  !  the wind temperature in [K].
  !
  !===============================================================  
  FUNCTION cmfgen_temperature(point)  RESULT(out)
    
    IMPLICIT NONE
    REAL                          :: out 
    TYPE(Vector), INTENT(IN) :: point
    !
    real(double) :: r
    
    r = modulus(point)
    if (r<cmfgen_opacity%Rmin) then ! inside of the star
       out = 1000.0d0
    else
       out = real(loginterp_dble(cmfgen_opacity%T, cmfgen_opacity%nd, cmfgen_opacity%R, r))
    end if

  END FUNCTION cmfgen_temperature


  
  !
  ! The interface routine to be used in generic routine in amr_mod.f90
  !
  
  SUBROUTINE cmfgen_mass_velocity(thisOctal,subcell)
    !  Assigings the density and velocity values to thisOctal at a given
    !  subcell using the density and the velocity functions as descrubed
    !  below.

    !    use inputs_mod
        
    IMPLICIT NONE

    TYPE(octal), INTENT(INOUT) :: thisOctal
    INTEGER, INTENT(IN) :: subcell
    !    
    TYPE(Vector) :: point
    
    REAL(oct) :: r    
        

    point = subcellCentre(thisOctal,subcell)
    r = modulus( point )   ! [10^10cm]
                      
    ! test if the point lies within the star
    IF ( r >= cmfgen_opacity%Rmin .AND. r <= cmfgen_opacity%Rmax) THEN
       thisOctal%inFlow(subcell) = .TRUE.       
       thisOctal%inStar(subcell) = .FALSE.
    ELSEIF (r < cmfgen_opacity%Rmin) THEN
       thisOctal%inFlow(subcell) = .FALSE.
       thisOctal%inStar(subcell) = .TRUE.
    ELSEIF (r > cmfgen_opacity%Rmax) THEN
       thisOctal%inFlow(subcell) = .FALSE.
       thisOctal%inStar(subcell) = .FALSE.
    ELSE
       thisOctal%inFlow(subcell) = .FALSE.
       thisOctal%inStar(subcell) = .FALSE.            
    END IF

    ! assiging density using a function in this module    
    thisOctal%rho(subcell) = cmfgen_density(point)   ! [g/cm^3]

    ! assiging velocity using a fuction in this module
    thisOctal%velocity(subcell) = cmfgen_velocity(point)  ! [c]

    ! assigining corner velocities using a function in this module
  IF ((thisoctal%threed).and.(subcell == 8)) &
       CALL VelocityCorners(thisOctal)
  IF ((thisoctal%twod).and.(subcell == 4)) &
       CALL VelocityCorners(thisOctal)
  IF ((thisoctal%oned).and.(subcell == 2)) &
       CALL VelocityCorners(thisOctal)
    

  END SUBROUTINE cmfgen_mass_velocity


  !
  ! The interface routine to be used in generic routine in amr_mod.f90
  !  
  SUBROUTINE calc_cmfgen_temperature(thisOctal,subcell)
  
    IMPLICIT NONE

    TYPE(octal), INTENT(INOUT) :: thisOctal
    INTEGER, INTENT(IN) :: subcell
    
    TYPE(Vector) :: point 

    point = subcellCentre(thisOctal,subcell)

    ! using the function in this module    
    thisOctal%temperature(subcell) = cmfgen_temperature(point)  ! [K]
    ! we will initialise the bias distribution
    !  --- I am not sure why this should be done here....
!    thisOctal%biasLine3D(subcell) = 1.0
    
  END SUBROUTINE calc_cmfgen_temperature



  SUBROUTINE VelocityCorners(thisOctal)
    ! store the velocity values at the subcell corners of an octal so
    !   that they can be used for interpolation.

    IMPLICIT NONE
  
    TYPE(octal), INTENT(INOUT) :: thisOctal

    real(oct)      :: x1, x2, x3
    real(oct)      :: y1, y2, y3
    real(oct)      :: z1, z2, z3
    


    if (thisOctal%threeD) then
       ! we first store the values we use to assemble the position vectors
    
       x1 = thisOctal%centre%x - thisOctal%subcellSize
       x2 = thisOctal%centre%x
       x3 = thisOctal%centre%x + thisOctal%subcellSize
       
       y1 = thisOctal%centre%y - thisOctal%subcellSize
       y2 = thisOctal%centre%y
       y3 = thisOctal%centre%y + thisOctal%subcellSize
       
       z1 = thisOctal%centre%z - thisOctal%subcellSize
       z2 = thisOctal%centre%z
       z3 = thisOctal%centre%z + thisOctal%subcellSize
       
       ! now store the 'base level' values
       
       thisOctal%cornerVelocity(1) = cmfgen_velocity(Vector(x1,y1,z1))
       thisOctal%cornerVelocity(2) = cmfgen_velocity(vector(x2,y1,z1))
       thisOctal%cornerVelocity(3) = cmfgen_velocity(vector(x3,y1,z1))
       thisOctal%cornerVelocity(4) = cmfgen_velocity(vector(x1,y2,z1))
       thisOctal%cornerVelocity(5) = cmfgen_velocity(vector(x2,y2,z1))
       thisOctal%cornerVelocity(6) = cmfgen_velocity(vector(x3,y2,z1))
       thisOctal%cornerVelocity(7) = cmfgen_velocity(vector(x1,y3,z1))
       thisOctal%cornerVelocity(8) = cmfgen_velocity(vector(x2,y3,z1))
       thisOctal%cornerVelocity(9) = cmfgen_velocity(vector(x3,y3,z1))
       
       ! middle level
       
       thisOctal%cornerVelocity(10) = cmfgen_velocity(vector(x1,y1,z2))
       thisOctal%cornerVelocity(11) = cmfgen_velocity(vector(x2,y1,z2))
       thisOctal%cornerVelocity(12) = cmfgen_velocity(vector(x3,y1,z2))
       thisOctal%cornerVelocity(13) = cmfgen_velocity(vector(x1,y2,z2))
       thisOctal%cornerVelocity(14) = cmfgen_velocity(vector(x2,y2,z2))
       thisOctal%cornerVelocity(15) = cmfgen_velocity(vector(x3,y2,z2))
       thisOctal%cornerVelocity(16) = cmfgen_velocity(vector(x1,y3,z2))
       thisOctal%cornerVelocity(17) = cmfgen_velocity(vector(x2,y3,z2))
       thisOctal%cornerVelocity(18) = cmfgen_velocity(vector(x3,y3,z2))
       
       ! top level
       
       thisOctal%cornerVelocity(19) = cmfgen_velocity(vector(x1,y1,z3))
       thisOctal%cornerVelocity(20) = cmfgen_velocity(vector(x2,y1,z3))
       thisOctal%cornerVelocity(21) = cmfgen_velocity(vector(x3,y1,z3))
       thisOctal%cornerVelocity(22) = cmfgen_velocity(vector(x1,y2,z3))
       thisOctal%cornerVelocity(23) = cmfgen_velocity(vector(x2,y2,z3))
       thisOctal%cornerVelocity(24) = cmfgen_velocity(vector(x3,y2,z3))
       thisOctal%cornerVelocity(25) = cmfgen_velocity(vector(x1,y3,z3))
       thisOctal%cornerVelocity(26) = cmfgen_velocity(vector(x2,y3,z3))
       thisOctal%cornerVelocity(27) = cmfgen_velocity(vector(x3,y3,z3))
    
    else if (thisOctal%twoD) then! 2D case
       
       ! we first store the values we use to assemble the position vectors
       
       x1 = thisOctal%centre%x - thisOctal%subcellSize
       x2 = thisOctal%centre%x
       x3 = thisOctal%centre%x + thisOctal%subcellSize
       
       z1 = thisOctal%centre%z - thisOctal%subcellSize
       z2 = thisOctal%centre%z
       z3 = thisOctal%centre%z + thisOctal%subcellSize
       
       ! now store the 'base level' values
       
       if (.not.associated (thisoctal%cornervelocity)) then
          allocate(thisOctal%cornerVelocity(1:9))
       endif
       thisOctal%cornerVelocity(1) = cmfgen_velocity(vector(x1,0.d0,z1))
       thisOctal%cornerVelocity(2) = cmfgen_velocity(vector(x2,0.d0,z1))
       thisOctal%cornerVelocity(3) = cmfgen_velocity(vector(x3,0.d0,z1))
       thisOctal%cornerVelocity(4) = cmfgen_velocity(vector(x1,0.d0,z2))
       thisOctal%cornerVelocity(5) = cmfgen_velocity(vector(x2,0.d0,z2))
       thisOctal%cornerVelocity(6) = cmfgen_velocity(vector(x3,0.d0,z2))
       thisOctal%cornerVelocity(7) = cmfgen_velocity(vector(x1,0.d0,z3))
       thisOctal%cornerVelocity(8) = cmfgen_velocity(vector(x2,0.d0,z3))
       thisOctal%cornerVelocity(9) = cmfgen_velocity(vector(x3,0.d0,z3))

    else ! one-d case
       ! we first store the values we use to assemble the position vectors
       
       x1 = thisOctal%centre%x - thisOctal%subcellSize
       x2 = thisOctal%centre%x
       x3 = thisOctal%centre%x + thisOctal%subcellSize
       
       ! now store the 'base level' values
       
       if (.not.associated (thisoctal%cornervelocity)) then
          allocate(thisOctal%cornerVelocity(1:3))
       endif

       thisOctal%cornerVelocity(1) = cmfgen_velocity(vector(x1,0.d0,0.d0))
       thisOctal%cornerVelocity(2) = cmfgen_velocity(vector(x2,0.d0,0.d0))
       thisOctal%cornerVelocity(3) = cmfgen_velocity(vector(x3,0.d0,0.d0))
    endif

  END SUBROUTINE VelocityCorners


  !
  ! Stores Rmin value (this does not have to be same as R(1) value.
  !
  subroutine put_cmfgen_Rmin(Rmin)
    implicit none
    real(double), intent(in) :: Rmin
    cmfgen_opacity%Rmin = Rmin
  end subroutine put_cmfgen_Rmin

  !
  !
  !
  function get_cmfgen_Rmin() RESULT(Rmin)
    implicit none
    real(double) :: Rmin
    Rmin = cmfgen_opacity%Rmin
  end function get_cmfgen_Rmin


  subroutine put_cmfgen_Rmax(Rmax)
    implicit none
    real(double), intent(in) :: Rmax
    cmfgen_opacity%Rmax = Rmax
  end subroutine put_cmfgen_Rmax

  !
  !
  !
  function get_cmfgen_Rmax() RESULT(Rmax)
    implicit none
    real(double) :: Rmax
    Rmax = cmfgen_opacity%Rmax
  end function get_cmfgen_Rmax



  recursive subroutine distort_cmfgen(thisOctal, grid)
    use inputs_mod, only : bigOmega, eddingtonGamma, alphaCAK, uniformStar
    type(gridtype) :: grid
    type(octal), pointer   :: thisOctal
    type(octal), pointer  :: child 
    integer :: subcell, i
    type(vector)  :: rvec
    real(double)::  mu, sintheta
    real(double) :: mDotFac, rhoFac, vFac
    real(double) :: tot, dmu, scaleFactor


    tot = 0.d0
    do i = 1, 1000
       mu = -1.d0 + 2.d0*dble(i-1)/999.d0
       dmu = 2.d0/999.d0
       sinTheta = sqrt(1.d0-mu**2)
       if (uniformStar) then
          mdotFac = (1.d0 - (bigOmega**2*sinTheta**2)/(1.d0-eddingtonGamma))**(1.d0-1.d0/alphaCAK) !eq 4
       else
          mdotFac = (1.d0 - (bigOmega**2*sinTheta**2))
       endif
       tot = tot + mDotFac * dmu
    enddo
    tot = tot * twoPi
    scaleFactor = fourPi / tot

    
    do subcell = 1, thisOctal%maxChildren
       if (thisOctal%hasChild(subcell)) then
          ! find the child
          do i = 1, thisOctal%nChildren, 1
             if (thisOctal%indexChild(i) == subcell) then
                child => thisOctal%child(i)
                call distort_cmfgen(child, grid)
                exit
             end if
          end do
          
       else
          
          rVec = subcellCentre(thisOctal, subcell)
          if (modulus(rVec) /= 0.d0) then
             mu = rVec%z/modulus(rVec)
          else
             mu = 0.d0
          endif
          sinTheta = sqrt(1.d0-mu**2)
          
! Dwarkadas and Owocki, 2002, ApJ 581, 1337



          if (uniformStar) then
! uniform star

             mdotFac = (1.d0 - (bigOmega**2*sinTheta**2)/(1.d0-eddingtonGamma))**(1.d0-1.d0/alphaCAK) !eq 4
             vFac = (1.d0 - (bigOmega**2 * sinTheta**2)/(1.d0-eddingtonGamma))**0.5d0 ! eq 7 

          else

! grav darkened star

             mdotFac = (1.d0 - (bigOmega**2*sinTheta**2))
             vFac = (1.d0 - (bigOmega**2 * sinTheta**2))**0.5d0 ! eq 7 

          endif

          rhoFac = scaleFactor * mDotFac / vFac

          
          thisOctal%rho(subcell) = thisOctal%rho(subcell) * rhoFac
          thisOctal%chiLine(subcell) = thisOctal%chiLine(subcell) * rhoFac**2
          thisOctal%etaLine(subcell) = thisOctal%etaLine(subcell) * rhoFac**2
          thisOctal%etaCont(subcell) = thisOctal%etaCont(subcell) * rhoFac**2
          thisOctal%ne(subcell) = thisOctal%ne(subcell) * rhoFac
          thisOctal%kappaAbs(subcell,1) = thisOctal%kappaAbs(subcell,1) * rhoFac**2
          thisOctal%kappaSca(subcell,1) = thisOctal%kappaSca(subcell,1) * rhoFac

          ! assigining corner velocities using a function in this module
          IF ((thisoctal%threed).and.(subcell == 8)) &
               CALL VelocityCorners(thisOctal)
          IF ((thisoctal%twod).and.(subcell == 4)) &
               CALL VelocityCorners(thisOctal)
          IF ((thisoctal%oned).and.(subcell == 2)) &
               CALL VelocityCorners(thisOctal)
          
       endif ! if (thisOctal%hasChild(subcell)) then
    enddo
    
  end subroutine distort_cmfgen
  !===================================================================
  ! Given a point and grid, this function returns
  ! the wind velocity (as a vector) in units of speed of light [c]. 
  ! 
  !====================================================================
  FUNCTION cmfgen_beta_velocity(r, Rstar) RESULT(out)
    
    IMPLICIT NONE

    real(double)                  :: out   ! [km/s]
    real(double), intent(in)      :: r     ! [10^10cm]
    real(double), intent(in) :: Rstar    ! 10^10cm   = 20 Rsun
    !
    real(double)  :: Vr
    !
    real(double), parameter :: V0    = 10.0d0    ! km/s
    real(double), parameter :: Vinf  = 2000.0d0  ! km/s
    real(double), parameter :: beta  = 1.0d0     ! [-]
    !
!    real(double), parameter :: Rstar=139.10d0      ! 10^10cm   = 20 Rsun

    !Rstar = cmfgen_opacity%Rmin

    !
    if (r<Rstar) then ! inside of the star
       vr = 1.0d-10   ! [kms]
    else
       Vr = V0 + (Vinf-V0)*(1.0d0-Rstar/r)**beta
       Vr = MAX(Vr, 1.0d0)    ! km/s
    end if

    out = Vr
    
  END FUNCTION cmfgen_beta_velocity





  !
  !
  !===================================================================
  ! Given a point and grid, this function returns the density in [g/cm^3] 
  ! at the point by interpolating the original zeus data.
  !    
  FUNCTION cmfgen_beta_density(r, Rstar) RESULT(out)
    
    IMPLICIT NONE

    REAL(double)                  :: out
    real(double), intent(in)      :: r   ! [10^10 cm]
    real(double), intent(in) :: Rstar    ! 10^10cm   = 20 Rsun
    !
    real(double), parameter ::  M_sun      = 1.9891d33 ! in [g]
    real(double), parameter ::  rho_min    = 1.0d-23 ! [g/cm^3]     
    real(double), parameter ::  rho_max    = 1.0d0   ! [g/cm^3]     
    real(double), parameter ::  Mdot       = 1.0d-6    ! [Msun/yr]     
    real(double), parameter ::  Mdot_cgs   = Mdot*(M_sun/(60.0d0*60.0d0*24.0d0*365.0d0))    ! [g/s]     
    real(double), parameter ::  pi         = 3.141593d0
    !
    !
    real(double) :: r_cgs, vr_cgs

    if (r< Rstar .or. r>cmfgen_opacity%Rmax) then 
       out = rho_min  ! just assign a small value and return
    else       
       ! using a routine in utils_mod
       r_cgs = r*1.0d10
       vr_cgs = cmfgen_beta_velocity(r, Rstar)*1.0d5
       out = Mdot_cgs / ( 4.0d0*pi*Vr_cgs*r_cgs*r_cgs )
       out = MAX(rho_min, out)
    end if

  END FUNCTION cmfgen_beta_density



  !
  !
  !
  !===============================================================
  !  Returns given point and grid, this function return 
  !  the wind temperature in [K].
  !
  !===============================================================  
  FUNCTION cmfgen_beta_temperature(r, Rstar)  RESULT(out)
    
    IMPLICIT NONE
    REAL                          :: out    ! [K]
    real(double), intent(in) :: r   ! 10^10 cm
    real(double), intent(in) :: Rstar    ! 10^10cm   = 20 Rsun
    !
!    real(double), parameter  ::  T_iso = 36.0d3  ! [K]
    real(double), parameter  ::  T_iso = 18.0d3  ! [K]

    
    if (r<Rstar) then ! inside of the star
       out = 1000.0d0
    else
       out = T_iso
       out = MAX(out, 3000.0)   ! limit the minimum temperature here
    end if

  END FUNCTION cmfgen_beta_temperature



end module cmfgen_class



