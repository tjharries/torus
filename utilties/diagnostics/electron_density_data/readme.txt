wang_OII contains a lookup table of values for electron density given the emission line ratio for [OII] 3729/3726.

Interpolate a given line ratio to find the density.

The table contains:


number density, num density + err, num density - err, log(Ne), log(Ne) + err, log(Ne) - err



These values are taken from Wang et al. 2004 A&A 427, 873-886


These values have to be determined observationally in between the limits of ne -> 0 and ne -> infinity
